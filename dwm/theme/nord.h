// colors taken from https://github.com/ThamognyaKodi/DWM-Nord-Theme

//static const char nord_polar_darkest_blue[] 		= "#2E3440";
//static const char nord_polar_darker_blue[] 			= "#3B4252";
//static const char nord_polar_lighter_dark_blue[] 	= "#434C5E";
//static const char nord_polar_lightest_dark_blue[] 	= "#4C566A";
//static const char nord_dark_white[] 				= "#D8DEE9";
//static const char nord_darker_white[] 				= "#E5E9F0";
//static const char nord_white[] 						= "#ECEFF4";
//static const char nord_white_dark[] 				= "#d8dee9";
//static const char nord_frost_light_blue[] 			= "#8FBCBB";
//static const char nord_frost_darker_light_blue[] 	= "#88C0D0";
//static const char nord_frost_lighter_dark_blue[]	= "#81A1C1";
//static const char nord_frost_dark_blue[] 			= "#5E81AC";
//static const char nord_red[] 						= "#BF616A";
//static const char nord_orange[] 					= "#D08770";
//static const char nord_yellow[] 					= "#EBCB8B";
//static const char nord_green[] 						= "#A3BE8C";
//static const char nord_purple[] 					= "#B48EAD";

// nord_white
static char norm_fg[] 								= "#ECEFF4";
// nord_polar_darkest_blue
static char norm_bg[] 								= "#2E3440";
// nord_polar_darkest_blue
static char norm_border[] 							= "#2E3440";

// nord_polar_lightest_dark_blue
static char sel_fg[] 								= "#4C566A";
// nord_frost_dark_blue
static char sel_bg[] 								= "#5E81AC";
// nord_red
static char sel_border[] 							= "#BF616A";
