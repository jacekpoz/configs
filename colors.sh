#!/bin/bash
if [[ "$#" -lt 1 ]]; then
	echo "pass a theme !!!" >&2
	exit 1
fi

theme_name=$@
# theme name in all lowercase
name=${theme_name,,}
# theme name with first letter uppercase
Name=${name^}

# copies of the above with spaces switched for hyphens
nameHyphen=${name//\ /-}

NameHyphen=${nameHyphen^}

not_found () { echo "$1 theme not found" >&2; }

replace_last_line () {
	if [[ "$#" -lt 2 ]]; then return 1; fi
	path=$1
	[[ -f "$path" ]] || return 1
	string=$2
	sed -i '$d' $path
	echo $string >> $path
	return 0
}

# wallpaper


# kitty 
kitty +kitten themes --reload-in=all $Name >/dev/null 2>&1 || not_found "kitty"

# alacritty just cause why not
alacritty-colorscheme apply "$name.yaml" >/dev/null 2>&1 || alacritty-colorscheme apply "$name.yml" >/dev/null 2>&1 || not_found "alacritty"

# nvim
# TODO change this to the native nvim --remote when it's usable
nvim-ctrl "colorscheme $name" >/dev/null 2>&1 || not_found "nvim"

nvim_theme_path='/home/jacek/.config/nvim/theme.vim'
replace_last_line $nvim_theme_path "colorscheme $name"

# cmus
[[ $(cmus-remote -C "colorscheme $name") != *Error* ]] || not_found "cmus"

# .Xresources (includes dwm)
xresources_path='/home/jacek/.Xresources'
xres_theme_path="/home/jacek/.themes/$Name/.Xresources"
[[ -f $xres_theme_path ]] && replace_last_line $xresources_path "#include \"$xres_theme_path\""

xrdb /home/jacek/.Xresources

# dwm (it uses .Xresources so has to be after it)

# TODO idk this seems weird maybe there's a better way to restart dwm but for now it's good
xdotool key super+shift+r

# rofi

user_themes_path='/home/jacek/.config/rofi/themes/'
themes_path='/usr/share/rofi/themes/'
rofi_theme_path='/home/jacek/.config/rofi/theme.rasi'
[[ -f "$themes_path/$name.rasi" ]] && replace_last_line $rofi_theme_path "@theme \"$themes_path/$name.rasi\"" || 
	[[ -f "$user_themes_path/$name.rasi" ]] && replace_last_line $rofi_theme_path "@theme \"$name\"" || not_found "rofi"
