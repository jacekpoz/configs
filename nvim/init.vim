call plug#begin()

" themes
Plug 'arcticicestudio/nord-vim'

Plug 'morhetz/gruvbox'

" end themes

Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' }

Plug 'lervag/vimtex'

Plug 'sheerun/vim-polyglot'

Plug 'cdelledonne/vim-cmake'

Plug 'tpope/vim-fugitive'

Plug 'vim-airline/vim-airline'

Plug 'vim-airline/vim-airline-themes'

Plug 'nacitar/a.vim'

Plug 'preservim/nerdtree' |
Plug 'Xuyuanp/nerdtree-git-plugin' |
Plug 'ryanoasis/vim-devicons'

Plug 'leafgarland/typescript-vim'
Plug 'peitalin/vim-jsx-typescript'

call plug#end()

source ~/.config/nvim/theme.vim

let g:carousel_enabled = 1

let g:airline_powerline_fonts = 1

if !exists('g:airline_symbols')
	let g:airline_symbols = {}
endif
let g:airline_symbols.space = "\ua0"

set termguicolors

let g:Hexokinase_highlighters = ['backgroundfull']

" set Vim-specific sequences for RGB colors
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

syntax enable

hi Normal guibg=NONE ctermbg=NONE

set number

filetype plugin on
filetype indent on

set autoread

command! W execute 'w !sudo tee % > /dev/null' <bar> edit!

set so=7

set ruler

set cmdheight=1

set hid

set ignorecase

set smartcase

set hlsearch

set incsearch

set showmatch

set noerrorbells
set novisualbell
set t_vb=
set tm=500

set encoding=utf8

set smarttab

set shiftwidth=4
set tabstop=4

set lbr
set tw=500

set ai
set si
set wrap

nnoremap <C-o> :NERDTreeToggle<CR>

"https://vim.fandom.com/wiki/Insert_newline_without_entering_insert_mode 
nmap OO O<Esc>
nmap oo o<Esc>

" vimtex stuff and things

call vimtex#init()

filetype plugin indent on

let g:vimtex_view_general_viewer = 'okular'
let g:vimtex_view_general_options = '--unique file:@pdf\#src:@line@tex'

" disable <C-p> that shit's annoying it fucking autocompletes and I press it instead of <C-[> often which I use as <Esc>
map <C-p> <Nop>

let g:NERDTreeGitStatusUseNerdFonts = 1
