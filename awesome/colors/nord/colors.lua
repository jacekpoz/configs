local colors = {}

colors.bg_normal 		= "#434C5E"
colors.bg_focus 		= "#5E81AC"
colors.bg_urgent 		= "#BF616A"
colors.bg_minimize 		= "#2E3440"
colors.bg_systray 		= "#434C5E"

colors.fg_normal 		= "#ECEFF4"
colors.fg_focus 		= "#4C566A"
colors.fg_urgent 		= "#ECEFF4"
colors.fg_minimize 		= "#ECEFF4"

colors.border_normal 	= "#434C5E"
colors.border_focus		= "#BF616A"
colors.border_marked 	= "#5E81AC"

return colors
