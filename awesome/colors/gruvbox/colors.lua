local colors = {}

colors.bg_normal 		= "#282828"
colors.bg_focus 		= "#FABD2F"
colors.bg_urgent 		= "#FB4934"
colors.bg_minimize 		= "#32302f"
colors.bg_systray 		= "#282828"

colors.fg_normal 		= "#EBDBB2"
colors.fg_focus 		= "#928374"
colors.fg_urgent 		= "#EBDBB2"
colors.fg_minimize 		= "#EBDBB2"

colors.border_normal 	= "#282828"
colors.border_focus		= "#FB4934"
colors.border_marked 	= "#FABD2F"

return colors
