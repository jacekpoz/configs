local gtable = require("gears.table")
local default_colors = require("colors.default")

local actual_colors = require("colors.pick_colors")

local final_colors = {}

gtable.crush(final_colors, default_colors)
gtable.crush(final_colors, actual_colors)

return actual_colors
