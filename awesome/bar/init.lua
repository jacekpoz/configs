local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")

local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local bar = require("bar.pick_bar")

screen.connect_signal("request::desktop_decoration", function(s) 

	local mytaglist = awful.widget.taglist {
		screen = s, 
		filter = bar.mytaglistargs.filter, 
		buttons = bar.mytaglistargs.buttons, 
	}

	local mylayoutbox = awful.widget.layoutbox(s)
	mylayoutbox:buttons(gears.table.join(
							awful.button({ }, 1, function () awful.layout.inc( 1) end),
							awful.button({ }, 3, function () awful.layout.inc(-1) end),
							awful.button({ }, 4, function () awful.layout.inc( 1) end),
							awful.button({ }, 5, function () awful.layout.inc(-1) end)))
	
	local mytasklist = awful.widget.tasklist {
		screen = s, 
		filter = bar.mytasklistargs.filter, 
		style = bar.mytasklistargs.style, 
		buttons = bar.mytasklistargs.buttons, 
	}

	awful.tag(bar.mytags, s, awful.layout.suit.tile)

	local mywibox = awful.wibar({
		screen = s, 
		width = dpi(s.geometry.width * bar.mywiboxargs.widthRatio),
		height = bar.mywiboxargs.height, 
		margins = {
			top = dpi(s.geometry.height * (bar.mywiboxargs.marginRatios.top)), 
			bottom = dpi(s.geometry.height * (bar.mywiboxargs.marginRatios.bottom)), 
			left = dpi(s.geometry.width * (bar.mywiboxargs.marginRatios.left)), 
			top = dpi(s.geometry.width * (bar.mywiboxargs.marginRatios.top)), 
		}, 
	})

	mywibox:setup {
		layout = wibox.layout.align.horizontal, 
		{
			layout = wibox.layout.fixed.horizontal, 
			bar.myseparator, 
			mytaglist, 
			bar.myseparator, 
			mylayoutbox, 
			bar.myseparator, 
		}, 
		mytasklist, 
		{
			layout = wibox.layout.fixed.horizontal, 
			table.unpack(bar.rightwidgets), 
		}, 
	}
end)
