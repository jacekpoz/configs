local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")

local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local batteryarc_widget = require("awesome-wm-widgets.batteryarc-widget.batteryarc")
local brightness_widget = require("awesome-wm-widgets.brightness-widget.brightness")
local volume_widget = require("awesome-wm-widgets.volume-widget.volume")
local cmus_widget = require("awesome-wm-widgets.cmus-widget.cmus")
local cpu_widget = require("awesome-wm-widgets.cpu-widget.cpu-widget")
local net_speed_widget = require("awesome-wm-widgets.net-speed-widget.net-speed")
local ram_widget = require("awesome-wm-widgets.ram-widget.ram-widget")

-- mouse buttons on the tags
local taglist_buttons = gears.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                )

-- mouse buttons on the open windows
local tasklist_buttons = gears.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  c:emit_signal(
                                                      "request::activate",
                                                      "tasklist",
                                                      {raise = true}
                                                  )
                                              end
                                          end),
                     awful.button({ }, 3, function()
                                              awful.menu.client_list({ theme = { width = 250 } })
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                          end))

local mytextclock = wibox.widget.textclock()
mytextclock.font = "JetBrains Mono 9"
mytextclock.format = "%F %T"
mytextclock.refresh = 1
mytextclock.timezone = "Europe/Warsaw"

local myseparator = wibox.widget.separator({
	forced_width = 5, 
	opacity = 0, 
})

local mypromptbox = awful.widget.prompt()

local mytags = { " ", " ", " ", " ", " ", " ", " ", " ", " " }

local mytaglistargs = {
	filter = awful.widget.taglist.filter.all, 
	buttons = taglist_buttons, 
}

local mytasklistargs = {
	filter = awful.widget.tasklist.filter.currenttags, 
	style = {
		shape = gears.shape.rounded_bar, 
	}, 
	buttons = tasklist_buttons,
}

local mywiboxargs = {
	widthRatio = 0.95, 
	height = dpi(24), 
	marginRatios = {
		top = 0.005, 
		bottom = 0.003, 
		left = 0, 
		right = 0, 
	},
}

local rightwidgets = {
	myseparator,
	wibox.widget.systray(),
	myseparator, 
	ram_widget({
		color_free = beautiful.bg_minimize, 
		color_buf = beautiful.bg_focus, 
		widget_show_buf = true, 
	}), 
	myseparator,
	cpu_widget({
		enable_kill_button = true,
	}),
	myseparator,
	brightness_widget({
		program = "brightnessctl",
		step = 1,
		base = 100,
		tooltip = true,
		font = "JetBrains Mono 6",
	}),
	myseparator,
	cmus_widget({
		font = "JetBrains Mono 9",
		space = 5,
		timeout = 1,
	}),
	myseparator,
	volume_widget({
		widget_type = "arc",
		step = 1,
	}),
	myseparator,
	net_speed_widget(),
	myseparator,
--		batteryarc_widget({
--			show_current_level = true,
--			font = "JetBrains Mono 6",
--			timeout = 1,
--			charging_color = "#34e034",
--		}),
	myseparator,
	mytextclock, 
	myseparator,
}

return {
	mytags = mytags, 
	mytaglistargs = mytaglistargs, 
	mytasklistargs = mytasklistargs, 
	mywiboxargs = mywiboxargs, 
	myseparator = myseparator, 
	rightwidgets = rightwidgets, 
}
