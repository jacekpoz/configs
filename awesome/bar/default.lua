local gears = require("gears")
local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")

local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local batteryarc_widget = require("awesome-wm-widgets.batteryarc-widget.batteryarc")
local brightness_widget = require("awesome-wm-widgets.brightness-widget.brightness")
local volume_widget = require('awesome-wm-widgets.volume-widget.volume')
local cmus_widget = require('awesome-wm-widgets.cmus-widget.cmus')
local cpu_widget = require("awesome-wm-widgets.cpu-widget.cpu-widget")
local net_speed_widget = require("awesome-wm-widgets.net-speed-widget.net-speed")

-- mouse buttons on the tags
local taglist_buttons = gears.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                )

-- mouse buttons on the open windows
local tasklist_buttons = gears.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  c:emit_signal(
                                                      "request::activate",
                                                      "tasklist",
                                                      {raise = true}
                                                  )
                                              end
                                          end),
                     awful.button({ }, 3, function()
                                              awful.menu.client_list({ theme = { width = 250 } })
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                          end))

awful.screen.connect_for_each_screen(function(s)

    -- Each screen has its own tag table.
    awful.tag({ " ", " ", " ", " ", " ", " ", " ", " ", " " }, s, awful.layout.suit.tile)

	s.mytextclock = wibox.widget.textclock()
	s.mytextclock.font = "JetBrains Mono 9"
	s.mytextclock.format = "%F %T"
	s.mytextclock.refresh = 1
	s.mytextclock.timezone = "Europe/Warsaw"
	s.myseparator = wibox.widget.separator({
		forced_width = 5,
		opacity = 0,
	})

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = taglist_buttons
    }
    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
		style 	= {
			shape = gears.shape.rounded_bar
		},
        buttons = tasklist_buttons,
    }

    -- Create the wibox
    s.mywibox = awful.wibar({
		screen = s,
		width = dpi(s.geometry.width * 0.95),
		margins = {
			top = 5,
			bottom = 5,
		},
	})

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
			s.myseparator,
            s.mytaglist,
			s.myseparator,
            s.mypromptbox,
			s.myseparator,
            s.mylayoutbox,
			s.myseparator,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
			s.myseparator,
            wibox.widget.systray(),
			s.myseparator,
			cpu_widget({
				enable_kill_button = true,
			}),
			s.myseparator,
			brightness_widget({
				program = "brightnessctl",
				step = 1,
				base = 100,
				tooltip = true,
				font = "JetBrains Mono 6",
			}),
			s.myseparator,
			cmus_widget({
				font = "JetBrains Mono 9",
				space = 5,
				timeout = 1,
			}),
			s.myseparator,
			volume_widget({
				widget_type = "arc",
				step = 1,
			}),
			s.myseparator,
			net_speed_widget(),
			s.myseparator,
--			batteryarc_widget({
--				show_current_level = true,
--				font = "JetBrains Mono 6",
--				timeout = 1,
--				charging_color = "#34e034",
--			}),
			s.myseparator,
            s.mytextclock, 
			s.myseparator,
        },
    }
end)
