local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local gears = require("gears")
local dpi = xresources.apply_dpi
local fs = require("gears.filesystem")
local theme_dir = fs.get_configuration_dir() .. "/theme/default"

local theme = {}

theme.icons 		= theme_dir .. "/icons/"
theme.font 			= "JetBrains Mono 8"

theme.useless_gap 	= dpi(2)
theme.border_width 	= dpi(2)

theme.taglist_square_size = dpi(5)

-- my own functions, not using them cause they get removed with each update =(
--theme.taglist_squares_sel = theme_assets.taglist_rects_sel(
--    taglist_rect_width, taglist_rect_height, theme.fg_normal
--)
--theme.taglist_squares_unsel = theme_assets.taglist_rects_unsel(
--    taglist_rect_width, taglist_rect_height, theme.fg_normal
--)

theme.layout_fairh 			= theme.icons .. "fairhw.png"
theme.layout_fairv  		= theme.icons .. "fairvw.png"
theme.layout_floating 		= theme.icons .. "floatingw.png"
theme.layout_magnifier 		= theme.icons .. "magnifierw.png"
theme.layout_max 			= theme.icons .. "maxw.png"
theme.layout_fullscreen 	= theme.icons .. "fullscreenw.png"
theme.layout_tilebottom 	= theme.icons .. "tilebottomw.png"
theme.layout_tileleft 		= theme.icons .. "tileleftw.png"
theme.layout_tile 			= theme.icons .. "tilew.png"
theme.layout_tiletop 		= theme.icons .. "tiletopw.png"
theme.layout_spiral  		= theme.icons .. "spiralw.png"
theme.layout_dwindle 		= theme.icons .. "dwindlew.png"
theme.layout_cornernw 		= theme.icons .. "cornernww.png"
theme.layout_cornerne 		= theme.icons .. "cornernew.png"
theme.layout_cornersw 		= theme.icons .. "cornersww.png"
theme.layout_cornerse 		= theme.icons .. "cornersew.png"

theme.icon_theme = nil

-- wibar
theme.wibar_border_width = dpi(0)
theme.wibar_border_radius = 7.5
theme.wibar_opacity = 0.85
theme.wibar_shape = function(cr, w, h) gears.shape.rounded_rect(cr, w, h, theme.wibar_border_radius) end

return theme
