local gtable = require("gears.table")
local theme_assets = require("beautiful.theme_assets")

local colors = require("colors")

local default_theme = require("theme.default")
local actual_theme = require("theme.pick_theme")

local final_theme = {}

gtable.crush(final_theme, default_theme)
gtable.crush(final_theme, actual_theme)

final_theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    final_theme.taglist_square_size, colors.fg_normal
)
final_theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    final_theme.taglist_square_size, colors.fg_normal
)

return final_theme
