local awful = require("awful")

programs = {
	"setxkbmap -layout pl", 
	"dunst", 
	"xbindkeys", 
	"flameshot", 
	"picom --experimental-backends", 
	"nitrogen --set-scaled --random --head=0 && nitrogen --set-scaled --random --head=1", 
}

for i = 1, #programs do
	awful.util.spawn(programs[i])
end
