# configs
some people call them dotfiles, others call them rice so I thought why not configs

most of this shit is stolen and then edited a bit so if you wanna steal this I'm not gonna cry but it would be cool if you credited me =D

# requirements

I'm running arch btw 🤓 so the packages are either from the arch repository or from aur and nowhere else but I also use a lot of `*-git` packages so you could just `git clone` them and build them yourself it's not that hard

| arch / aur package | link to package | link to website / repo | other shit |
| ------------------ | --------------- | ---------------------- | ---------- |
| awesome-git | https://aur.archlinux.org/packages/awesome-git | https://awesomewm.org/ | |
| awesome-wm-widgets | null | https://github.com/streetturtle/awesome-wm-widgets | you don't need to install it it's already in my repo just wanted to give credit |
| kitty-git | https://aur.archlinux.org/packages/kitty-git | https://sw.kovidgoyal.net/kitty/ | |
| picom-git | https://aur.archlinux.org/packages/picom-git | https://github.com/yshui/picom | there's a million forks but Idk this works for me |
| JetBrains Mono with Ligatures (patched) from nerd-fonts | https://aur.archlinux.org/packages/nerd-fonts-jetbrains-mono | https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/JetBrainsMono/Ligatures | |
| zsh | https://archlinux.org/packages/?name=zsh | https://www.zsh.org/ | |
| oh-my-zsh | https://github.com/ohmyzsh/ohmyzsh#basic-installation | https://ohmyz.sh/ | this is the only package I didn't install from arch / aur Idk why I just curled the script |
| nvim-git | https://aur.archlinux.org/packages/neovim-git | https://neovim.io/ | |
| dunst | https://archlinux.org/packages/?name=dunst | https://dunst-project.org/ | |
| rofi-git | https://aur.archlinux.org/packages/rofi-git | https://github.com/davatorium/rofi | |
| slock | https://git.suckless.org/slock/ | https://tools.suckless.org/slock/ | |

I think that's it Idk