alias \
	cl="clear" \
	cp="cp -ivr" \
	mv="mv -iv" \
	rm="rm -vrf" \
	exa="exa -lha --git" \
	ls="ls -A --color=auto" \
	yt="yt-dlp -P \"~/Downloads/\" -v -o \"%(title)s.%(ext)s\" -w --write-description --embed-metadata --embed-chapters --embed-subs --embed-thumbnail" \
	yta="yt -x --audio-quality 0" \
	u="sudo pacman -Syu && paru" \
	pkg="sudo pacman -S" \
	qpkg="pacman -Q" \
	spkg="pacman -Ss" \
	rpkg="sudo pacman -Runs" \
	pkgdep="pacman -Sii" \
	ipkg="pacman -Qii" \
	untar="tar -xvf" \
	untargz="tar -xzf" \
	b64="base64 --decode" \
	mnt="udisksctl mount -b" \
	umnt="udisksctl unmount -b" \
	w="curl wttr.in" \
	canto="canto-curses" \
	aliases="nvim ~/.oh-my-zsh/custom/aliases.zsh" \
	nvim-config="nvim ~/.config/nvim/init.vim" \
	stfu="shutdown now" \
	kys="stfu" \
	bc="bc -lq"
